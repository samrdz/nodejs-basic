var http = require('http');

function servidorSimple(request, response){
  var cabeceras = {
    'Content-Type' : 'text/plain'
  }
  response.writeHead(200, cabeceras)
  response.end('Hello World');
}

http.createServer(servidorSimple).listen(3005);
  console.log('simple up');
